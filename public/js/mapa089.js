$(document).ready(function(){

	function setPositionMap(elem)
	{
		$('#contacts-info .address').html(elem.attr('data-adress'));
		$('#contacts-info .call u').html(elem.attr('data-phone'));

		var gps = elem.attr('data-gps').split(',');

		console.log(gps);

		myMap.panTo(
			// Координаты нового центра карты
			[gps[1]*1, gps[0]*1], {
				/* Опции перемещения:
				 разрешить уменьшать и затем увеличивать зум
				 карты при перемещении между точками
				 */
				flying: false
			}
		)
	}

	$('#map_changer').change(function ()
	{
		setPositionMap($(this).find("option:selected").first());
	});
	$('#map_changer').on('blur', function ()
	{
		setPositionMap($(this).find("option:selected").first());
	});

});

//map partner
var myMap;
ymaps.ready(init);
function init(){


	myMap = new ymaps.Map("map", {
		center: [43.219217, 76.929218],
		zoom: 16,
		controls: ['zoomControl'],
		behaviors: ['drag','multiTouch'],
	});

	if($(window).width() < 760){
		myMap.behaviors.disable('drag');
	}

	$('#map_changer option').each(function(){
		var i = 0;
		var gps = $(this).attr('data-gps');
		if (gps != '')
		{
			gps = $(this).attr('data-gps').split(',');

			var myPlacemark = new ymaps.Placemark([gps[1], gps[0]] ,
				{},
				{ iconLayout: 'default#image',
					iconImageHref: 'svg/placeholder.svg',
					iconImageSize: [27, 34],
					iconImageOffset: [-20, -47] });
			myMap.geoObjects.add(myPlacemark);
			i=i+1;
		}
	});
}