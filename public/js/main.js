$(document).ready(function(){
    var windowWdith = $(window).width();

    if (windowWdith < 1025) {
        var tab = true;
    } else {
        var tab = false;
    }

    $(window).resize(function() {
        windowWdith = $(window).width();
        if (windowWdith < 1025 && tab == false) {
            location.reload();
        } else if (windowWdith >= 1025 && tab == true) {
            location.reload();
        }
    });

    $( function() {
        $( "#slider-range-min-1" ).slider({
            range: "min",
            value: 1,
            min: 100000,
            step: 100000,
            max: 50000000,
            slide: function( event, ui ) {
                $("#sum-credit").val(ui.value+' '+'' );
            }
        });
        $( "#sum-credit" ).val($( "#slider-range-min-1").slider( "value" )+" "+"");
    });

    $(".tabs_menu li").click(function() {
        if (!$(this).hasClass("active")) {
            var i = $(this).index();
            $(".tabs_menu li.active").removeClass("active");
            $(".tabs .active").hide().removeClass("active");
            $(this).addClass("active");
            $($(".tabs").children(".info")[i]).fadeIn(500).addClass("active");
        }
    });

    $(".tabs_menu2 li").click(function() {
        if (!$(this).hasClass("active")) {
            var i = $(this).index();
            $(".tabs_menu2 li.active").removeClass("active");
            $(".tabs2 .active").hide().removeClass("active");
            $(this).addClass("active");
            $($(".tabs2").children(".info2")[i]).fadeIn(500).addClass("active");
        }
    });


    $(".go-bottom").on("click touchstart" , function (event) {

        $('html,body').animate({
            scrollTop: $('#aboutbond').offset().top
        }, 700, 'swing');
    });

    if($(window).width() > 1025){

        $('.acc-every .acc-h').click(function(){
            $(this).next('.acc-body').slideToggle(300);
            $(this).find('.acc-arrow').toggleClass('rotate');
            $(this).parents('.acc-every').toggleClass('change');
        });

        $('.to-next a').click(function(e) {
            e.preventDefault();
            $('html').animate({
                scrollTop: $('#aboutbond').offset().top
            }, 700);
        });
        $('.naverh').click(function(e) {
            e.preventDefault();
            $('html').animate({
                scrollTop: $('.section1').offset().top
            }, 700);
        });
        $(".to-next a").on("click touchstart" , function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: $('#aboutbond').offset().top
            }, 700, 'swing');
        });
        $(".naverh").on("click touchstart" , function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: $('.section1').offset().top
            }, 700, 'swing');
        });



        $(".menu7 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('#aboutbond').offset().top
            }, 700, 'swing');
        });

        $(".menu2 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.section3').offset().top
            }, 700, 'swing');
        });
        $(".menu3 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.zigzag').offset().top
            }, 700, 'swing');
        });
        $(".menu4 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.section5').offset().top
            }, 700, 'swing');
        });
        $(".menu5 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.question').offset().top
            }, 700, 'swing');
        });
        $(".menu6 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.cities').offset().top
            }, 700, 'swing');
        });


        $('.section4').mousemove(function(e){
            var x = -(e.pageX + this.offsetLeft) / 20;
            var y = -(e.pageY + this.offsetTop) / 20;
            $(this).css('background-position', x + 'px ' +'0px');
        });

        $('.forma').mousemove(function(e){
            var x = -(e.pageX + this.offsetLeft) / 20;
            var y = -(e.pageY + this.offsetTop) / 20;
            $(this).css('background-position', x + 'px ' + '0px');
        });


    }

    //anchor

    if($(window).width() < 1025){

        $(".menu7 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('#aboutbond').offset().top
            }, 700, 'swing');
        });
        $(".menu2 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.section3').offset().top
            }, 700, 'swing');
        });
        $(".menu3 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.zigzag').offset().top
            }, 700, 'swing');
        });
        $(".menu4 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.section5').offset().top
            }, 700, 'swing');
        });
        $(".menu5 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.question').offset().top
            }, 700, 'swing');
        });
        $(".menu6 a").on("click touchstart" , function (event) {

            $('html,body').animate({
                scrollTop: $('.cities').offset().top
            }, 700, 'swing');
        });


        $('.acc-every .acc-h').click(function(){
            $(this).next('.acc-body').slideToggle(300);
            $(this).find('.acc-arrow').toggleClass('rotate');
            $(this).parents('.acc-every').toggleClass('change');
        });
    }

    //Sliders options
    $('.owl-slider').owlCarousel({
        center:false,
        loop:true,
        margin:0,
        stagePadding: 0,
        mouseDrag:true,
        touchDrag:true,
        autoplay:false,
        autoplayTimeout:3000,
        smartSpeed:500,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        responsive:{
            0:{
                items:2,
                nav: true,
            },
            600:{
                items:4
            },
            1000:{
                items:5
            }
        },
    });

    $('.owl-slider2').owlCarousel({
        center:false,
        loop:true,
        margin:0,
        stagePadding: 0,
        mouseDrag:true,
        touchDrag:true,
        autoplay:false,
        autoplayTimeout:3000,
        smartSpeed:500,
        autoplayHoverPause: true,
        nav: true,
        dots: true,
        responsive:{
            0:{
                items:1,
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        },
    });


    //mask input
    $('.tele').mask('+7 (999) 999 99 99');

    $( function() {
        var item = $('.invest-scroll-area');
        var content = $('.invest');
        var maxScroll = content.width() - item.width();

        $( "#slider-range" ).slider({
            range: "min",

            slide: function( e, ui ) {
                var currentScroll = maxScroll*(ui.value/100);

                item.scrollLeft(currentScroll);


            },
            create: function(){
                $('#slider-range span').addClass('infinite');
                $('#slider-range span').addClass('animated3');
                $('#slider-range span').addClass('shake2');
                $('#slider-range span').mouseover(function(){
                    $('#slider-range span').removeClass('shake2');
                });
                $('#slider-range span').mouseleave(function(){
                    $('#slider-range span').addClass('shake2');
                });
            }


        });
    });

    //responsive js
    if($(window).width() < 1024){


    }
    if($(window).width() > 1024){


    }

    if($(window).width() < 1025){




        $(window).scroll(function(){
            var scrollo = $(window).scrollTop();
            var secHeights = $('#forma').offset().top;

            if(scrollo > secHeights){

                var inNur = function(){
                    $('.mob-type-nurtas').show();
                    $('.mob-type-nurtas').addClass('animated2');
                    $('.mob-type-nurtas').addClass('fadeInLeft');
                }

                var swing = function(){
                    $('.mob-type-nurtas').removeClass('fadeInLeft');
                    $('.mob-type-nurtas').removeClass('animated2');
                    $('.mob-type-nurtas').addClass('animated2');
                    $('.mob-type-nurtas').addClass('wobble');
                }

                var outNur = function(){
                    $('.mob-type-nurtas').removeClass('wobble');
                    $('.mob-type-nurtas').removeClass('animated2');
                    $('.mob-type-nurtas').addClass('animated2');
                    $('.mob-type-nurtas').addClass('fadeOutRight');
                }

                var hideNur = function(){
                    $('.mob-type-nurtas').hide();
                    $('.mob-type-form').show();
                    $('.mob-type-form').addClass('animated2');
                    $('.mob-type-form').addClass('fadeInUp');
                }


                var animate = function(){
                    var done = $('#forma').attr('done');
                    if(done == 0){
                        setTimeout(function(){
                            inNur();
                        },100);
                        setTimeout(function(){
                            swing();
                        },1500);
                        setTimeout(function(){
                            outNur();
                        },2500 );

                        setTimeout(function(){
                            hideNur();
                        },3000);
                        $('#forma').attr('done', '1');
                    }


                }

                animate();


            }

        });


        $('.text-black').css({'font-size': '12px'});

        $('.menu-list').insertAfter('.bb');
        // $('.mon1').insertAfter('.monsters').hide();

        // $('.to-next').insertAfter('.monsters .mon3');
        $('.push a').insertBefore('.push span');


        $('.list-ul li').removeClass('animated2');

        $('#map').insertAfter('.mob-map');

        $('.instruc').insertAfter('.summer2');

        $('.burger').click(function(){

            $('.burger-menu').fadeIn();
            $('.burger-menu').addClass('animated');
            $('.burger-menu').removeClass('slideOutRight');
            $('.burger-menu').addClass('slideInRight');

        });

        $('.burger-menu #menu li').click(function(){

            $('.burger-menu').fadeOut();
            $('.burger-menu').addClass('animated');
            $('.burger-menu').removeClass('slideInRight');
            $('.burger-menu').addClass('slideOutRight');

        });
        $('.burger-menu .close').click(function(){

            $('.burger-menu').fadeOut();
            $('.burger-menu').addClass('animated');
            $('.burger-menu').removeClass('slideInRight');
            $('.burger-menu').addClass('slideOutRight');

        });


        $('.switch .two').click(function(){
            $('.switch .one').removeClass('active');
            $('.switch .two').addClass('active');
            $('.list1').removeClass('active');
            $('.list2').addClass('active');
        });

        $('.switch .one').click(function(){
            $('.switch .two').removeClass('active');
            $('.switch .one').addClass('active');
            $('.list2').removeClass('active');
            $('.list1').addClass('active');
        });



        $('.mob-smi-img').click(function(){
            $(this).toggleClass('active');
            $(this).next('.smi-text').slideToggle();
        });

    }



    $('#menu li .lang-changer li').hide();
    $('#menu li .lang-changer li.active2').show();

    $('.lang-changer li').click(function(){

        $('.lang-changer li').removeClass('active2');
        $(this).addClass('active2');

        if($('.lang-changer').hasClass('clicked')){

            $('.lang-changer').removeClass('clicked');
            $('.lang-changer li').not('.lang-changer li.active2').slideUp();


        }
        else{
            $('.lang-changer').addClass('clicked');
            $('.lang-changer li').slideDown();

            $('.lang-changer li.active2').first();
        }


    });

    $('.logos-wrapper img').on('mouseenter', function() {
        $(this).css({'animation-name':'pulse','animation-delay':0});
    });


});



