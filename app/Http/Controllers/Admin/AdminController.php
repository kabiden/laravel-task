<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\Form;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.layouts.forms')->with('forms', Form::orderBy('id', 'DESC')->get());
    }

    public function login()
    {
        return view('admin.layouts.login');
    }

    public function auth(Request  $request) {
        $loginData = [
            'email' => $request->post('email'),
            'password' => $request->post('password'),
        ];
        if(Auth::attempt($loginData)) {
            return redirect()->to(route('forms'));
        } else {
            return redirect()->back();
        }
    }

    public function logout()
    {
        Auth::guard()->logout();
        return redirect()->to(route('login'));
    }

    public function delete($iID)
    {
        Form::find($iID)->delete();
        return back()->withInput();
    }
}
