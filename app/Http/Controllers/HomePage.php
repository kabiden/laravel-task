<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Form;

class HomePage extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function add_form(Request $request)
    {
        $validator = $this->getValidationFactory()->make($request->all(), [
            'full_name' => 'required|min:2|max:255',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required|min:3|max:18'
        ]);

        if ($validator->fails())
            return response()->json($validator->errors(), 400);

        if ($oForm = Form::create($request->all()))
            return response()->json($oForm, 201);
        else
            return response()->json($oForm, 500);

    }
}
