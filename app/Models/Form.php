<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Form
 * @package App\Models
 * @property string full_name
 * @property string email
 * @property string phone
 */

class Form extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'forms_data';
    protected $fillable = ['full_name', 'email', 'phone'];
}
