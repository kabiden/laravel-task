<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomePage@index');
Route::get('/admin/login', 'Admin\AdminController@login')->name('login');
Route::post('/admin/auth', 'Admin\AdminController@auth')->name('auth');

Route::middleware(['auth', 'auth.admin'])->group(function () {
    Route::post('/admin/logout', 'Admin\AdminController@logout')->name('logout');
    Route::post('/admin/forms/delete/{id}', 'Admin\AdminController@delete')->name('delete');
    Route::get('/admin/forms', 'Admin\AdminController@index')->name('forms');
});
