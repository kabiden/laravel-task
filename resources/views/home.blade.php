
<!DOCTYPE html>
<html lang="ru">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5ZNM74M');</script>
    <!-- End Google Tag Manager -->

    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Эффект 6% годовых в долларах и никаких кошмаров!">
    <meta charset="UTF-8">
    <title>Бонды "Фридом Финанс"</title>


    <link rel="shortcut icon" href="img/fav.png?ver=2" />

    <!-- <link rel="stylesheet" type="text/css" href="css/jquery.fullPage.css" /> -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css" />
    <link rel="stylesheet" type="text/css" href="css/owl.theme.default.css" />
    <link rel="stylesheet" type="text/css" href="css/owl.theme.green.css" />
    <link rel="stylesheet" type="text/css" href="css/remodal.css">
    <link rel="stylesheet" type="text/css" href="css/remodal-default-theme.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css?ver=100" />
    <link rel="stylesheet" type="text/css" href="css/animate.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" />
    <link rel="stylesheet" type="text/css" href="css/main.css?ver=101" />
    <link rel="stylesheet" type="text/css" href="css/new.css?ver=101" />
    <link rel="stylesheet" type="text/css" href="css/tab.css?v=24071305" />
    <link rel="stylesheet" type="text/css" href="css/mobile.css?ver=100" />

    <link rel="stylesheet" type="text/css" href="css/new-v-2.0.css?ver=101" />

    <script type="text/javascript" src="js/jquery-3.2.0.min.js"></script>
    <script src="js/jquery.migrate.3.0.0.js"></script>
    <script type="text/javascript" src="js/maskedinput.js"></script>
    <script type="text/javascript" src="js/owl.carousel.js"></script>
    <script type="text/javascript" src="js/owl.carousel2.thumbs.js"></script>
    <script type="text/javascript" src="js/remodal.min.js"></script>
    <!-- <script type="text/javascript" src="js/jquery.localscroll-1.2.7-min.js"></script>
      <script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script> -->
    <!-- <script type="text/javascript" src="js/jquery.fullPage.min.js"></script> -->
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
    <!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script> -->
    <script type="text/javascript" src="js/jquery.fancybox.min.js"></script>
    <!-- amCharts javascript sources -->
    <!-- <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> -->
    <script type="text/javascript" src="js/amcharts.js"></script>
    <script type="text/javascript" src="js/serial.js"></script>

    <script type="text/javascript" src="js/main.js?v=24071305"></script>

    <script>
        $(window).on('load', function () {
            var $preloader = $('#page-preloader'),
                $spinner   = $preloader.find('.spinner');
            $spinner.fadeOut();
            $preloader.delay(350).fadeOut('slow');
        });
        setTimeout(
            function() {
                $('#page-preloader').delay(350).fadeOut('slow');
                // setTimeout(function(e) {$.fancybox.open({src: '#bond-pop-up'});}, 5000);
            },
            3000);
    </script>



</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZNM74M"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<div id="page-preloader">
		<span class="spinner">
			<div class="loader-line-wrap">
				<div class="loader-line">

				</div>
			</div>
		</span>
</div>

<div class="mainpage">

    <header class="header scrolled">
        <div class="wrapper">
            <div class="row">
                <div class="header-row">
                    <div class="table-cell">
                        <div class="logo">
                            <a href="#">
                                <span></span>
                            </a>
                        </div>
                        <div class="menu-list">
                            <ul id="menu" class="desc-menu">
                                <li data-menuanchor="Бонды" class="menu7"><a href="#Бонды">Выгоды</a></li>
                                <li data-menuanchor="Решение" class="menu2"><a href="#Решение">Параметры</a></li>
                                <li data-menuanchor="Инвестировать" class="menu3"><a href="#Инвестировать">Как инвестировать</a></li>
                                <li data-menuanchor="Компания" class="menu4"><a href="#Компания">О компании</a></li>
                                <!-- <li data-menuanchor="Кошмары" class="active menu1"><a href="#Кошмары">Кошмары</a></li> -->
                                <li data-menuanchor="Вопросы" class="menu5"><a href="#Вопросы">Вопрос/ответ</a></li>
                                <li data-menuanchor="Контакты" class="menu6"><a href="#Контакты">Контакты</a></li>
                                <li class="language">
                                    <!--<ul class="lang-changer">
                                        <li class="active2"><a href="" onclick="return false">Рус</a></li>
                                        <li><a href="kz/">Каз</a></li>
                                    </ul> /-->
                                </li>
                            </ul>
                        </div>
                        <div class="burger">
                            <span></span>
                        </div>
                        <div class="phone">
                            <a href="tel:7555">
                                <span>7555</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="header-relative"></div>

    <div class="burger-menu">
        <div class="close">
            <span></span>
        </div>
        <div class="burger-logo">
            <a href="">
                <span></span>
            </a>
        </div>
        <div class="bb"></div>
        <ul id="menu">
            <li class="active menu7"><a href="#">Выгоды</a></li>
            <!-- <li class="menu1"><a href="#">Кошмары</a></li> -->
            <li class="menu2"><a href="#">Параметры</a></li>
            <li class="menu3"><a href="#">Как инвестировать</a></li>
            <li class="menu4"><a href="#">О компании</a></li>
            <li class="menu5"><a href="#">Вопрос/ответ</a></li>
            <li class="menu6"><a href="#">Контакты</a></li>
            <li class="active2"><a href=""  onclick="return false">Рус</a></li>
            <li><a href="kz/">Каз</a></li>
        </ul>
    </div>

    <div class="naverh">
        <a href="">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve"> <g> <path style="fill:#000000;" d="M24,3.125c11.511,0,20.875,9.364,20.875,20.875S35.511,44.875,24,44.875S3.125,35.511,3.125,24S12.489,3.125,24,3.125 M24,0.125C10.814,0.125,0.125,10.814,0.125,24S10.814,47.875,24,47.875S47.875,37.186,47.875,24S37.186,0.125,24,0.125L24,0.125z"></path> </g> <path style="fill:#000000;" d="M25.5,36.033c0,0.828-0.671,1.5-1.5,1.5s-1.5-0.672-1.5-1.5V16.87l-7.028,7.061c-0.293,0.294-0.678,0.442-1.063,0.442 c-0.383,0-0.766-0.146-1.058-0.437c-0.587-0.584-0.589-1.534-0.005-2.121l9.591-9.637c0.281-0.283,0.664-0.442,1.063-0.442 c0,0,0.001,0,0.001,0c0.399,0,0.783,0.16,1.063,0.443l9.562,9.637c0.584,0.588,0.58,1.538-0.008,2.122 c-0.589,0.583-1.538,0.58-2.121-0.008l-6.994-7.049L25.5,36.033z"></path> </svg>
        </a>
    </div>

    <div id="fullpage">
        <div class="section section1"  data-anchor="Главная">
            <div class="main-content">
                <div class="wrapper">
                    <div class="main-content__row">
                        <div class="main-content__col-1">
                            <img src="img/6.5.png" class="main-content__img">
                        </div>
                        <div class="main-content__col-2">
                            <h1 class="main-content__title">БОНДЫ <br>FREEDOM FINANCE</h1>
                            <div class="main-content__subtitle">ПО-НАСТОЯЩЕМУ ЦЕННЫЕ БУМАГИ</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper main__footer">
                <div class="col-6">
                    <button class="btn main__btn btn_color_green" data-fancybox data-src="#mon1">Купить бонды</button>
                    <a href="https://bonds.kz/docs/INFOBOND(RU)7.pdf" class="btn main__btn btn_color_green_light go-bottom" target="_blank">Узнать подробнее</a>
                </div>
                <div class="col-6">
                    <p class="main__license">Бонды «Фридом Финанс» зарегистрированны Национальным <br>Банком РК НИН KZP02Y03F541<br>Кредитный рейтинг «B-» от S&P Global Ratings.
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>


        <div class="mon1" id="mon1" style="display: none;">
            <form action="" class="mons-form form">
                <span>Оставить заявку</span>
                <div class="req-inputs">
                    <input class="hfield" type="text" placeholder="Не запoлнять" name="NAME">
                    <input class="hfield" type="text" placeholder="Не запoлнять" name="EMAIL">
                    <input class="hfield" type="text" placeholder="Не запoлнять" name="COMMENT">
                </div>
                <input required type="text" placeholder="ФИО" class="input1 form-fio" name="form[fio]">
                <input required type="text" placeholder="Телефон" class="input1 tele form-phone" name="form[tfn]">
                <input required type="email" placeholder="Email" class="input1 form-email" name="form[eml]">
                <button>Отправить</button>
            </form>
        </div>


        <div class="section fp-normal-scroll" id="aboutbond" data-anchor="Бонды" >
            <div class="about_content" style="display: none">
                <div class="wrapper">
                    <div class="row">
                        <div class="about_1">
                            <h2 class="about_title">Бонды – </h2>
                            <h3 class="about_subtitle">современное <br>название <br>облигаций</h3>
                            <div class="rc-podrobno">
                                <a data-fancybox data-type="iframe" href="https://www.youtube.com/embed/_kKJmB0h95c">
                                    <p></p>
                                    <span>Что такое бонды?</span>
                                </a>
                            </div>
                        </div>
                        <div class="about_2">
                            <div class="about_2_content">
                                <div class="heading">
                                    <span>простыми словами</span>
                                </div>
                                <p class="about_2-text">Покупая бонды, вы даете компании в долг
                                    и через определенное время получаете вложенную сумму обратно. При этом
                                    за пользование вашими деньгами компания регулярно платит вам заранее определенное вознаграждение.
                                    <img src="svg/paper-plane.svg" class="paper_plane slideInLeft wow" data-wow-duration="1s">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wrapper">
                    <div class="row">
                        <div class="about_3">
                            <div class="about_3_content">
                                <div class="rc-podrobno">
                                    <a data-fancybox href="docs/INFOBOND(RU)7.pdf" target="_blank">
                                        <p></p>
                                        <span>Детальная информация <br>о бондах* </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="svg/about_left.svg" alt="" class="about__image_left">
                <img src="svg/about_bottom.svg" alt="" class="about__image_bottom">
            </div>
            <div class="about_advantage">
                <div class="wrapper">
                    <div class="row">
                        <div class="heading">
                            <span>Для чего нужны бонды</span>
                        </div>
                        <div class="advantages__list">
                            <div class="advantages__items animated2 wow fadeInUp" data-wow-delay="0.2s">
                                <div class="advantages__content advantages__content_left">
                                    <div class="advantage__img">
                                        <img src="svg/boss.svg" width="76" height="86" alt="Бонды для инвестора">
                                    </div>
                                    <p><b>Для инвестора</b> бонды – это <br>инструмент сохранения <br>и приумножения капитала</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="advantages__items animated2 wow fadeInUp" data-wow-delay="0.4s">
                                <div class="advantages__content advantages__content_right">
                                    <div class="advantage__img mr15">
                                        <img src="svg/company.svg" width="82" height="82" alt="Бонды для компании">
                                    </div>
                                    <p style="padding-top: 10px;"><b>Компании</b>  привлекают с помощью <br>бондов средства для дальнейшего <br>роста и развития в соответствии <br>со своей стратегией </p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="advantages__items animated2 wow fadeInUp" data-wow-delay="0.6s">
                                <div class="advantages__content advantages__content_left">
                                    <div class="advantage__img">
                                        <img src="svg/mir_bank.svg" width="76" height="76" alt="Бонды для компании">
                                    </div>
                                    <p style="padding-top: 0;"><b>Правительства стран</b> покрывают дефицит <br>гос. бюджета, финансируют проекты <br>государственного масштаба и общие <br>правительственные расходы</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="advantages__items animated2 wow fadeInUp" data-wow-delay="0.8s">
                                <div class="advantages__content advantages__content_right">
                                    <div class="advantage__img mr15">
                                        <img src="svg/gov.svg" width="84" height="84" alt="Бонды для компании">
                                    </div>
                                    <p style="padding-top: 5px;"><b>Муниципальные власти</b><br> вкладывают в инфраструктуру, местные <br>и городские проекты, проводят <br>региональные программы</p>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about_safety">
                <div class="wrapper">
                    <div class="row">
                        <div class="heading">
                            <span>Это надежно</span>
                        </div>
                        <div class="safety-content">
                            <div class="safety-text">
                                <p>На фондовом рынке бонды испокон веков считаются классическим, консервативным инструментом вложений – наравне с акциями. Более того, если говорить о надежности, бонды традиционно считаются надежнее акций.</p>
                            </div>
                        </div>
                        <div class="safety-graph">
                            <div class="graph1 graph wow">
                                <div class="graph-value fadeIn wow" data-wow-delay=".8s">$76 <br>трлн</div>
                            </div>
                            <div class="graph2 graph wow" data-wow-delay="0.3s">
                                <div class="graph-value fadeIn wow" data-wow-delay="1.3s">$100+ <br>трлн</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about_numbers">
                <div class="wrapper">
                    <div class="row">
                        <div class="countrys-list">
                            <div class="country country1">
                                <div class="country_sphere">
                                    <img src="img/numbers-usa.png" class="country-sphere">
                                    <img src="img/numbers-usa-mobile.png" class="country-sphere-mobile">
                                    <div class="country_content">
                                        <h4>109 000</h4>
                                        <pre>  наименований <br>    корпоративных <br>      бондов</pre>
                                    </div>
                                    <div class="country-logos-list">
                                        <div class="logos-wrapper">
                                            <img src="img/usa-logo1.png" class="bounceIn wow" data-wow-delay="0.2s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/usa-logo2.png" class="bounceIn wow" data-wow-delay="0.4s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/usa-logo3.png" class="bounceIn wow" data-wow-delay="0.6s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/usa-logo4.png" class="bounceIn wow" data-wow-delay="0.4s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/usa-logo5.png" class="bounceIn wow" data-wow-delay="0.2s">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="country country2">
                                <div class="country_sphere">
                                    <img src="img/numbers-kz.png" class="country-sphere">
                                    <img src="img/numbers-kz-mobile.png" class="country-sphere-mobile">
                                    <div class="country_content">
                                        <h4>218</h4>
                                        <pre>  наименований <br>    корпоративных <br>      бондов</pre>
                                    </div>
                                    <div class="country-logos-list">
                                        <div class="logos-wrapper">
                                            <img src="img/kz-logo1.png" class="kz-logo bounceIn wow" data-wow-delay="0.2s">
                                            <img src="img/kz-logo1-mob.png" class="kz-logo-mob bounceIn wow" data-wow-delay="0.2s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/kz-logo2.png" class="kz-logo bounceIn wow" data-wow-delay="0.4s">
                                            <img src="img/kz-logo2.png" class="kz-logo-mob bounceIn wow" data-wow-delay="0.4s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/kz-logo3.png" class="kz-logo bounceIn wow" data-wow-delay="0.6s">
                                            <img src="img/kz-logo3.png" class="kz-logo-mob bounceIn wow" data-wow-delay="0.6s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/kz-logo4.png" class="kz-logo bounceIn wow" data-wow-delay="0.4s">
                                            <img src="img/kz-logo4.png" class="kz-logo-mob bounceIn wow" data-wow-delay="0.4s">
                                        </div>
                                        <div class="logos-wrapper">
                                            <img src="img/kz-logo5.png" class="kz-logo bounceIn wow" data-wow-delay="0.2s">
                                            <img src="img/kz-logo5.png" class="kz-logo-mob bounceIn wow" data-wow-delay="0.2s">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about_benefit">
                <div class="wrapper">
                    <div class="heading">
                        <span>Это выгодно</span>
                    </div>
                    <div class="row">
                        <div class="benefit benefit1">
                            <p>Сравним бонды с другим классическим инструментом вложений – депозитами. Эти продукты похожи своей простотой: деньги можно внести и оставить на несколько лет, никаких активных действий не требуется.</p><br>
                            <p>Разница в том, что при открытии депозита заключается депозитный договор с банком, а при покупке бондов – брокерский договор с инвестиционной компанией. </p>
                        </div>
                        <div class="benefit benefit2">
                            <img src="img/tree.png">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section4 fp-normal-scroll" data-anchor="Сравнение">
            <div class="sec3-2">
                <div class="wrapper">
                    <div class="row row1">
                        <div class="sec3-2-content">

                            <div class="switch">
                                <div class="one active">бонды freedom finance </div>
                                <div class="two">валютные депозиты</div>
                            </div>

                            <div class="list1 active">
                                <span class="list-head">бонды freedom finance</span>
                                <div class="list-ul">
                                    <ul>
                                        <li class=" animated2 wow fadeInLeft" data-wow-delay="0.2s"><p><span>Высокая доходность (к погашению):</span>
                                                6% годовых в долларах США - в 4 раза превышает ставки по валютным депозитам.

                                            </p></li>
                                        <li class=" animated2 wow fadeInLeft" data-wow-delay="0.4s"><p><span>Стабильность:</span> вознаграждение фиксированное на весь срок, выплачивается каждые полгода.</p></li>
                                        <!-- <li class=" animated2 wow fadeInLeft" data-wow-delay="0.6s"><p><span>Доллар и тенге на одной стороне:</span> никакой головной боли по поводу возможной девальвации национальной валюты</p></li> -->
                                        <!-- <li class=" animated2 wow fadeInLeft" data-wow-delay="0.6s"><p><span>Доступность:</span> порог входа - 100 тыс. тенге. </p></li> -->
                                        <li class=" animated2 wow fadeInLeft" data-wow-delay="0.6s"><p><span>Ваше остается вашим:</span> налоги с дохода по бондам не взимаются.</p></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="list2">
                                <span class="list-head">Валютные депозиты </span>
                                <div class="list-ul">
                                    <ul>
                                        <li class=" animated2 wow fadeInRight" data-wow-delay="0.2s"><p><span>Низкая доходность:</span><br>
                                                не более 1,5% годовых
                                            </p></li>
                                        <li class=" animated2 wow fadeInRight" data-wow-delay="0.4s"><p><span>Возможность пересмотра
											ставок</span><br> (в т.ч. в сторону
                                                понижения)
                                            </p></li>
                                        <li class=" animated2 wow fadeInRight" data-wow-delay="0.6s"><p><span>Отсутствие гибкости:</span>
                                                невыгодные условия при
                                                досрочном изъятии средств
                                            </p></li>
                                        <li class=" animated2 wow fadeInRight" data-wow-delay="0.8s"><p><span>Необходимость дважды менять валюту, нередко по невыгодному курсу</span>
                                            </p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section3 fp-normal-scroll" data-anchor="Решение">
            <div class="sec3-1">
                <div class="wrapper">
                    <div class="row">
							<span class="rewenie-h">
								ПАРАМЕТРЫ БОНДОВ
							</span>
                        <div class="rewenie-content">
                            <div class="rc1">
                                <div class="rc1-h">
                                    <span>Индексированные к доллару купонные облигации АО «Фридом Финанс».</span>
                                    <p>Фиксированные 6% годовых в долларах США.</p>
                                </div>
                                <div class="rc-tab">
                                    <ul class="tabs_menu">
                                        <li class="active">Характеристика</li>
                                        <li>Параметры участия</li>
                                    </ul>
                                    <div class="tabs">
                                        <div class="info active">
                                            <ul>
                                                <li><p>Валюта обязательств: <span>привязка к доллару США</span></p></li>
                                                <!-- <li><p>Номинальная стоимость одного бонда: <span>эквивалент $3,04</span></p></li> -->
                                                <li><p>Срок обращения: <span>3 года (2018-2021</span>)</p></li>
                                                <li><p>Доходность к погашению: <span>6% годовых в долларах США (фиксированная доходность)</span></p></li>
                                            </ul>
                                            <span class="tab-italic">* Бонды (от англ. bonds) – второе название облигаций </span>
                                        </div>
                                        <div class="info">
                                            <ul>
                                                <li><p>Возможность продажи до срока погашения: <span>есть.</span><br>
                                                        Продажа осуществляется на бирже по рыночной цене.</p></li>
                                                <li><p>Возможность дополнительных взносов: <span>есть. </span><br>
                                                        Покупка бондов осуществляется на бирже.</p></li>
                                                <div class="hide-lee"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rc2">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <!-- <div class="section fp-normal-scroll tablica" data-anchor="Таблица">
            <div class="sec3-3">
                <div class="wrapper">
                    <div class="row">
                        <div class="prognoz-content">
                            <div class="heading">
                                <span>прогноз Эffeкт 8</span>
                            </div>
                            <div class="summaries">
                                <div class="summer1">
                                    <div class="first-sum">
                                        <span class="sum-vloj">Сумма вложения</span>
                                        <div class="range_1_child"><div id="slider-range-min-1"></div></div>
                                        <div class="sum-credit">
                                            <input type="text" id="sum-credit">
                                            <span>КЗТ</span>
                                            <p class="start-sum">100 000</p>
                                            <p class="end-sum">50 000 000</p>
                                        </div>
                                        <div class="instruc">
                                            <span class="my-money">- Мои сбережения</span>
                                            <span class="dollar">- Курс доллара</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="summer2">
                                    <span class="sum-vloj">Курс $ в период вложений</span>
                                    <div class="year3-prognoz">
                                        <span>Ваш прогноз через 1 год:</span>
                                        <div class="y3p-input">
                                            <input type="number" value="332" id="future_kurs_input">
                                        </div>
                                        <p>тг</p>
                                        <a onclick="setNewGraphFuture();" style="cursor: pointer">Показать</a>
                                    </div>
                                    <div class="remember">
                                        <span>Вспомнить девальвацию 2014 г</span>
                                        <a onclick="setNewGraph(kurs2014);" style="cursor: pointer">Показать</a>
                                    </div>
                                    <div class="remember">
                                        <span>Вспомнить девальвацию 2015 г</span>
                                        <a onclick="setNewGraph(kurs2015);" style="cursor: pointer">Показать</a>
                                    </div>
                                </div>
                            </div>
                            <div id="chartdiv" style="width: 100%; height: 400px; background-color: #FFFFFF; margin: 0 auto;" ></div>
                            <script type="text/javascript">

                                if($(window).width() <= 450){

                                    $('#chartdiv').height('220');
                                }

                                var kurs2013 = [
                                    {'date':'2013-01','kurs':154.06},
                                    {'date':'2013-02','kurs':155.50},
                                    {'date':'2013-03','kurs':184.08},
                                    {'date':'2013-04','kurs':182.06},
                                    {'date':'2013-05','kurs':182.06},
                                    {'date':'2013-06','kurs':183.50},
                                    {'date':'2013-07','kurs':183.52},
                                    {'date':'2013-08','kurs':153.47},
                                    {'date':'2013-09','kurs':152.81},
                                    {'date':'2013-10','kurs':153.81},
                                    {'date':'2013-11','kurs':154.23},
                                    {'date':'2013-12','kurs':153.68},
                                    {'date':'2014-01','kurs':154.06}
                                ];
                                var kurs2014 = [
                                    {'date':'2014-01','kurs':154.06},
                                    {'date':'2014-02','kurs':155.50},
                                    {'date':'2014-03','kurs':184.08},
                                    {'date':'2014-04','kurs':182.06},
                                    {'date':'2014-05','kurs':182.06},
                                    {'date':'2014-06','kurs':183.50},
                                    {'date':'2014-07','kurs':183.52},
                                    {'date':'2014-08','kurs':183.28},
                                    {'date':'2014-09','kurs':182.00},
                                    {'date':'2014-10','kurs':181.90},
                                    {'date':'2014-11','kurs':180.87},
                                    {'date':'2014-12','kurs':180.87},
                                    {'date':'2015-01','kurs':182.35}
                                ];
                                var kurs2015 = [
                                    {'date':'2015-01','kurs':182.35},
                                    {'date':'2015-02','kurs':184.45},
                                    {'date':'2015-03','kurs':185.05},
                                    {'date':'2015-04','kurs':185.65},
                                    {'date':'2015-05','kurs':185.80},
                                    {'date':'2015-06','kurs':185.95},
                                    {'date':'2015-07','kurs':186.20},
                                    {'date':'2015-08','kurs':187.45},
                                    {'date':'2015-09','kurs':237.66},
                                    {'date':'2015-10','kurs':270.89},
                                    {'date':'2015-11','kurs':279.18},
                                    {'date':'2015-12','kurs':307.40},
                                    {'date':'2016-01','kurs':339.47}
                                ];
                                var kurs2018 = [
                                    {'date':'2017-06','kurs':312.26},
                                    {'date':'2017-07','kurs':322.27},
                                    {'date':'2017-08','kurs':326.74},
                                    {'date':'2017-09','kurs':337.04},
                                    {'date':'2017-10','kurs':341.19},
                                    {'date':'2017-11','kurs':334.71},
                                    {'date':'2017-12','kurs':331.22},
                                    {'date':'2018-01','kurs':332.33},
                                    {'date':'2018-02','kurs':322.90},
                                    {'date':'2018-03','kurs':320.30},
                                    {'date':'2018-04','kurs':318.31},
                                    {'date':'2018-05','kurs':327.25},
                                    {'date':'2018-06','kurs':329.13}
                                ];

                                function generateNewData(inputSum,kurs) {

                                    var output = [];
                                    var summ = inputSum/kurs[0].kurs;

                                    output.push({
                                        "date":kurs[0].date,
                                        "column-1": kurs[0].kurs,
                                        "column-2": inputSum,
                                    })

                                    for (var i = 1; i <= 12; i++)
                                    {
                                        summ = summ + (inputSum/kurs[i].kurs)*(0.08/12);

                                        output.push({
                                            "date":kurs[i].date,
                                            "column-1": kurs[i].kurs,
                                            "column-2": Math.ceil(summ*kurs[i].kurs),
                                        })
                                    }

                                    return output;
                                }
                                function getRandomArbitrary(min, max) {
                                    return Math.random() * (max - min) + min;
                                }
                                function setNewGraph(kurs)
                                {
                                    chart.dataProvider = generateNewData($("#sum-credit").val()*1,kurs);
                                    chart.validateData();
                                }
                                function setNewGraphFuture(kurs)
                                {
                                    var baseKurs = kurs2018[kurs2018.length - 1].kurs;
                                    var baseDate = kurs2018[kurs2018.length - 1].date;
                                    baseDate = baseDate.split('-');
                                    baseDate[0] = baseDate[0]*1;
                                    baseDate[1] = baseDate[1]*1;
                                    var futureKurs = $("#future_kurs_input").val()*1;
                                    var futureKursDelta = (futureKurs - baseKurs)/12;

                                    var newKurs = [];

                                    newKurs.push({
                                        date:baseDate[0]+'-'+baseDate[1],
                                        kurs:baseKurs
                                    });

                                    for (var i = 1; i <= 11; i++)
                                    {
                                        currentKurs = baseKurs + getRandomArbitrary(futureKursDelta-0.5,futureKursDelta+0.5);
                                        newKurs.push({
                                            date:baseDate[0]+'-'+(baseDate[1]+i),
                                            kurs:Math.round(currentKurs,2)
                                        })
                                        baseKurs = currentKurs;
                                    }
                                    newKurs.push({
                                        date:baseDate[0]+'-'+(baseDate[1]+i+1),
                                        kurs:futureKurs
                                    });

                                    chart.dataProvider = generateNewData($("#sum-credit").val()*1,newKurs);
                                    chart.validateData();
                                }

                                var chart = AmCharts.makeChart("chartdiv",
                                    {
                                        "type": "serial",
                                        "categoryField": "date",
                                        "dataDateFormat": "YYYY-MM",
                                        "theme": "default",
                                        "synchronizeGrid":true,
                                        "categoryAxis":
                                            {
                                                "equalSpacing": true,
                                                "minPeriod": "MM",
                                                "parseDates": true,
                                                "startOnAxis": true,
                                                "position": "left",
                                                "minorGridEnabled": true
                                            }
                                        ,
                                        "chartCursor": {
                                            "enabled": true,
                                            "categoryBalloonDateFormat": "MMM YYYY",
                                            "graphBulletSize": 1,
                                        },
                                        "trendLines": [],
                                        "graphs": [
                                            {
                                                "valueAxis": "ValueAxis-1",
                                                "bullet": "round",
                                                "bulletBorderColor": "#F9B000",
                                                "bulletBorderThickness": 0,
                                                "bulletColor": "#F9B000",
                                                "bulletSize": 30,
                                                "customBullet": "img/eclipse1.png",
                                                "fillColors": "#F9B000",
                                                "id": "AmGraph-1",
                                                "lineColor": "#F9B000",
                                                "lineThickness": 3,
                                                "maxBulletSize": 80,
                                                "precision": -6,
                                                "showBulletsAt": "open",
                                                "tabIndex": -4,
                                                "title": "graph 1",
                                                "valueField": "column-1"
                                            },
                                            {
                                                "valueAxis": "ValueAxis-2",
                                                "balloonColor": "#397B23",
                                                "bullet": "round",
                                                "bulletBorderColor": "#397B23",
                                                "bulletBorderThickness": 0,
                                                "bulletColor": "#397B23",
                                                "bulletSize": 30,
                                                "customBullet": "img/eclipse2.png",
                                                "fillColors": "#397B23",
                                                "id": "AmGraph-2",
                                                "lineColor": "#397B23",
                                                "lineThickness": 3,
                                                "tabIndex": 2,
                                                "title": "graph 2",
                                                "valueField": "column-2"
                                            }
                                        ],
                                        "guides": [],
                                        "valueAxes": [
                                            {
                                                "axisTitleOffset": 5,
                                                "id": "ValueAxis-1",
                                                "autoGridCount": false,
                                                "title": "",
                                                "position": "left",
                                            },{
                                                "axisTitleOffset": 5,
                                                "id": "ValueAxis-2",
                                                "autoGridCount": false,
                                                "title": "",
                                                "position": "right",
                                            }
                                        ],
                                        "allLabels": [],
                                        "balloon": {},
                                        "titles": [
                                            {
                                                "id": "Title-1",
                                                "size": 20,
                                                "text": ""
                                            }
                                        ],
                                        "dataProvider": generateNewData(100000,kurs2018)
                                    }
                                );

                                chart.dataProvider = generateNewData(100000,kurs2018);
                                chart.validateData();

                                $(document).ready(function(){

                                    if($(window).width() < 792){

                                        chart.graphs[0].bulletSize = 15;
                                        chart.graphs[1].bulletSize = 15;
                                        chart.validateData();
                                    }

                                    $( "#slider-range-min-1" ).on( "slidechange", function( event, ui )
                                    {
                                        chart.dataProvider = generateNewData(ui.value*1,kurs2018);
                                        chart.validateData();
                                    });

                                });

                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> -->

        <div class="section smibond fp-normal-scroll" data-anchor="СМИ">
            <div class="wrapper">
                <div class="row">
                    <div class="smi-content">
                        <div class="heading">
                            <span>сми о бондах</span>
                        </div>
                        <div class="smi-tabs">
                            <ul class="tabs_menu2">
                                <li class="active"><img src="img/s2.png" alt=""></li>
                                <li><img src="img/forbes.png" alt=""></li>
                                <li><img src="img/s1.png" alt=""></li>
                                <li><img src="img/logos/info-buro.svg" height="80px" alt=""></li>
                                <!-- <li><img src="img/s4.png" alt=""></li>
                                                  <li><img src="img/s5.png" alt=""></li> -->
                            </ul>
                            <div class="tabs2">
                                <div class="info2 active">
                                    <div class="mob-smi-img">
                                        <img src="img/s2.png" alt="">
                                    </div>
                                    <div class="smi-text">
                                        <span class="smi-h">Тимур Турлов: «На Западе люди предпочитают бонды депозитам из-за высокой доходности»</span>
                                        <p>4 июня на Казахстанской фондовой бирже стартовали торги облигациями (бондами), выпущенными инвестиционной компанией «Фридом Финанс» на сумму в 10 млрд тенге. Срок облигаций 3 года, ставка вознаграждения 7% годовых. Облигации выпущены в тенге, но их стоимость привязана к курсу доллара США и приобрести их может любой человек или компания. Более подробно о преимуществах этого инструмента в интервью «Къ» рассказал генеральный директор «Фридом Финанс» Тимур Турлов.
                                            <a href="https://www.kursiv.kz/news/finansy/timur-turlov-na-zapade-ludi-predpocitaut-bondy-depozitam-iz-za-vysokoj-dohodnosti/" target="blank"> Посмотреть детально</a></p>
                                    </div>
                                </div>
                                <div class="info2">
                                    <div class="mob-smi-img">
                                        <img src="img/forbes.png" alt="">
                                    </div>
                                    <div class="smi-text">
                                        <span class="smi-h">Бонды для народа: как выгодно вложить и хорошо заработать</span>
                                        <p>Один из богатейших людей в мире, легендарный американский инвестор Уоррен Баффет (Forbes оценивает его состояние в $78 млрд) говорил: «Инвестируйте только в то, что вы понимаете, и по правильной цене». На сегодняшний день рекомендации финансовых аналитиков США таковы: они считают, что в инвестиционном портфеле акции не должны превышать 35%, недвижимость — 10%, а вот бонды (облигации) должны составлять порядка 50%. Депозиты не должны превышать 5% - их рассматривают как средство сохранения денег, а не как инвестиционный инструмент. В Казахстане же, для человека, имеющего сбережения, фондовый рынок зачастую остается «терра инкогнита» - неведомой территорией. А между тем, по итогам прошлого года, на корпоративных ценных бумагах можно было неплохо заработать. При этом в Казахстане частные инвесторы не платят налог на прибыль, полученную от такого способа инвестирования. <a class="smi-desc" href="https://forbes.kz/finances/investment/bondyi_dlya_naroda_kak_vyigodno_vlojit_i_horosho_zarabotat/" data-fancybox data-type="iframe"> Посмотреть детально</a>
                                            <a class="smi-mob" href="https://m.forbes.kz/article/174567" data-fancybox data-type="iframe"> Посмотреть детально</a></p>
                                    </div>
                                </div>
                                <div class="info2">
                                    <div class="mob-smi-img">
                                        <img src="img/s1.png" alt="">
                                    </div>
                                    <div class="smi-text">
                                        <span class="smi-h">Идеальный инструмент для начинающего инвестора</span>
                                        <p>Инвестиции в бонды – наиболее надежное вложение средств на рынке ценных бумаг. Фактически это заем, т. е., покупая бонды, инвестор кредитует компанию на определенный срок, по окончании которого она возвращает ему всю заимствованную сумму плюс фиксированный процент. В отличие от акций, риск дефолта по долговым ценным бумагам сведен к минимуму. Владельцам бондов гарантированы возврат долга и выплата доходов в полном объеме. Все это обеспечивается активами заемщика, т. е. даже в случае банкротства компании инвесторы все равно получат назад свои деньги. Более того, держателям бондов вернут активы раньше, чем владельцам простых акций. <a class="smi-desc" href="https://kapital.kz/finance/69793/kak-zarabotat-na-bondah.html" data-fancybox data-type="iframe"> Посмотреть детально</a>
                                            <a class="smi-mob" href="https://kapital.kz/finance/69793/kak-zarabotat-na-bondah.html" data-fancybox data-type="iframe"> Посмотреть детально</a></p>
                                    </div>
                                </div>
                                <div class="info2">
                                    <div class="mob-smi-img">
                                        <img src="img/logos/info-buro.svg" height="80px" alt="">
                                    </div>
                                    <div class="smi-text">
                                        <span class="smi-h">Мечтаете о миллионе долларов? Есть варианты стать к нему ближе</span>
                                        <p>Известный американский эксперт в вопросах финансовой грамотности Дэвид Рэмси однаждый сказал: "Или ты будешь управлять своими деньгами, или их отсутствие будет управлять тобой". В Казахстане люди привыкли, что деньги можно заработать, только если из года в год ходить в офис каждый день и кропотливо выполнять указания и прихоти начальника. Если стараться больше всех и не опаздывать, то можно кроме зарплаты получить ещё и премию, а когда-нибудь через пару лет рассчитывать на повышение дохода. <a class="smi-desc" href="https://informburo.kz/cards/mechtaete-o-millione-dollarov-est-varianty-stat-k-nemu-blizhe.html" data-fancybox data-type="iframe"> Посмотреть детально</a>
                                            <a class="smi-mob" href="https://informburo.kz/cards/mechtaete-o-millione-dollarov-est-varianty-stat-k-nemu-blizhe.html" data-fancybox data-type="iframe"> Посмотреть детально</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="section bond fp-normal-scroll" data-anchor="Инвестировать">
            <div class="wrapper">
                <div class="row">
                    <div class="zigzag">
							<span class="zigzag-heading">
								Как купить бонды:
							</span>
                        <div class="zigzag1">
                            <div class="zz1">
                                <img src="img/w1.png" alt="">
                                <span class="zig-h">Шаг 1</span>
                                <span class="zig-span">Открыть брокерский счет</span>
                                <p class="zig-p">Вы открываете брокерский счет с возможностью торговли на <span>бирже KASE</span>. Процедура займет не более часа.
                                    Заключается брокерский договор. Необходимые документы: удостоверение личности и реквизиты счета в банке.</p>
                            </div>
                        </div>
                        <div class="zigzag2">
                            <div class="zz2">
                                <img src="img/w2.png" alt="">
                                <span class="zig-h">Шаг 2</span>
                                <span class="zig-span">Пополнить счет</span>
                                <p class="zig-p">Пополняете счет денежными средствами через безналичный банковский перевод.</p>
                            </div>
                        </div>
                        <div class="zigzag3">
                            <div class="zz3">
                                <img src="img/w3.png" alt="">
                                <span class="zig-h">Шаг 3</span>
                                <span class="zig-span">Подать заявление</span>
                                <p class="zig-p">Через инвестиционного консультанта подаете письменный приказ на покупку бондов.</p>
                            </div>
                        </div>
                        <div class="zigzag4">
                            <div class="zz4">
                                <img src="img/w4.png" alt="">
                                <span class="zig-h">Шаг 4</span>
                                <span class="zig-span">Зачисление бондов</span>
                                <p class="zig-p">Бонды будут зачислены на ваш брокерский счет.
                                    Купонное вознаграждение будет начисляться на счет раз в полгода.
                                    Вы будете регулярно получать отчет с отражением текущей цены бондов.
                                </p>
                            </div>
                        </div>

                        <div class="zigzag-forma">
                            <span>Купить бонды</span>
                            <!-- там внизу попап с названием popup1 скрыт -->
                            <form class="form">
                                <div class="req-inputs">
                                    <input class="hfield" type="text" placeholder="Не запoлнять" name="NAME">
                                    <input class="hfield" type="text" placeholder="Не запoлнять" name="EMAIL">
                                    <input class="hfield" type="text" placeholder="Не запoлнять" name="COMMENT">
                                </div>
                                <input required type="text" placeholder="ФИО" class="input1 form-fio" name="full_name">
                                <input required type="text" placeholder="Телефон" class="input1 tele form-phone" name="phone">
                                <input required type="email" placeholder="Email" class="input1 form-email" name="email">
                                <button>Отправить</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="section section5 fp-normal-scroll" data-anchor="Компания">
            <div class="wrapper">
                <div class="row">
                    <div class="about">
                        <div class="heading">
                            <span>О компании</span>
                        </div>
                        <div class="about-content">
                            <div class="about-text">
                                <p><span>«Фридом Финанс»</span> – крупнейший инвестиционный брокер Центральной Азии, дающий доступ к фондовым рынкам Казахстана, России, США и Европы.
                                    <br><br>

                                    Компания входит в международную группу Freedom Holding. Акции холдинговой компании размещены на <span><a target="_blank" href="https://www.nasdaq.com/market-activity/stocks/frhc">Nasdaq Capital Market</a></span> и находятся в публичном доступе для инвесторов под символом FRHC.


                                    <br><br>
                                    Холдинг регулируется Комиссией по ценным бумагам и биржам (SEC, США).
                                    <br><br>
                                    Собственный капитал группы: $134 119 000. Суммарные клиентские активы: $1 300 000 000.
                                    <br><br>
                                    На рынке с 2008 года.
                                </p>
                            </div>
                            <div class="about-list">
                                <div class="al-inner animated2 wow fadeInUp" data-wow-delay="0.2s">
                                    <div class="al-img">
                                        <img src="img/l1.png" alt="">
                                    </div>
                                    <p><span>Штат сотрудников: 1100 человек</span> Команда со средним опытом в финансовой сфере более 10 лет</p>
                                </div>
                                <div class="al-inner animated2 wow fadeInUp" data-wow-delay="0.4s">
                                    <div class="al-img">
                                        <img src="img/l2.png" alt="">
                                    </div>
                                    <p><span>76 офисов по всему миру.</span><br>
                                        Представительства в Кыргызстане, Казахстане, России, Украине, США, Кипре
                                    </p>
                                </div>
                                <div class="al-inner animated2 wow fadeInUp" data-wow-delay="0.6s">
                                    <div class="al-img">
                                        <img src="img/l3.png" alt="">
                                    </div>
                                    <p><span>Более 127 000 клиентов</span>. Тысячи причин находить для Вас лучшие решения</p>
                                </div>
                                <div class="al-inner animated2 wow fadeInUp" data-wow-delay="0.8s">
                                    <div class="al-img">
                                        <img src="img/l4.png" alt="">
                                    </div>
                                    <p class="underlined"><a href="https://src.kz/download/files/licenziya_novaya_ot_02_10_2018_g_.pdf" data-caption=""  data-fancybox="gallery">
                                            Лицензия #3.2.238/15 от 02 октября 2018 года на осуществление деятельности на рынке ценных бумаг, выданная Национальным банком РК.
                                        </a></p>
                                </div>
                                <div class="al-inner animated2 wow fadeInUp" data-wow-delay="1s">
                                    <div class="al-img">
                                        <img src="img/l5.png" alt="">
                                    </div>
                                    <p>Все финансовые средства хранятся на личном счете клиента в Центральном Депозитарии</p>
                                </div>
                                <div class="al-inner animated2 wow fadeInUp" data-wow-delay="1.2s">
                                    <div class="al-img">
                                        <img src="img/al4-2.png" alt="">
                                    </div>
                                    <p>Рейтинг "B-/B" от S&P Global Ratings. Прогноз "Стабильный".</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="partners">
                        <div class="heading">
                            <span>Партнеры</span>
                        </div>
                        <div class="par-slider">
                            <div class="owl-carousel owl-theme owl-slider">
                                <div class="item">
                                    <div class="table-cell">
                                        <!-- <img src="img/logos/1PNG.png" alt="" class="gray">
                                        <img src="img/logos/1PNG2.png" alt="" class="orig"> -->
                                        <img src="img/logos/1PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/2PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/3PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/4PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/5PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/6PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/7PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/8PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/9PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/10PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/11PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/12PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/13PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/14PNG.png" alt="">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="table-cell">
                                        <img src="img/logos/15PNG.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section section6 fp-normal-scroll" data-anchor="Директор">
            <div class="wrapper">
                <div class="row">
                    <div class="timur">
                        <div class="timur-h">
                            <div class="hidden-timur">

                            </div>
                            <span>Тимур Турлов </span>
                            <p>Генеральный директор ИК «Фридом Финанс»</p>
                        </div>
                        <div class="timur-text">
                            <p>Сегодня АО «Фридом Финанс» - самый динамично развивающийся брокер на рынке акций не только Казахстана, но и Центральной Азии, России и Украины. Более половины всех инвесторов, торгующих акциями в РК – наши клиенты. И мы очень хорошо понимаем, насколько сейчас становятся востребованными продукты с интересной фиксированной доходностью в долларах США. Сегодня мы можем предложить простой продукт, выпущенный по законодательству РК нашей инвестиционной компанией, входящей в американский публичный холдинг, чьи акции обращаются в США. Это одно из лучших соотношений по риску и доходу, подкрепленное нашей репутацией одного из наиболее эффективных и прозрачных инвестиционных холдингов.</p>
                        </div>
                    </div>
                    <div class="timur-img">
                        <img src="img/timur4.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="section section2 fp-normal-scroll" data-anchor="Кошмары" id="section2">
            <div class="owl-slider2 owl-carousel owl-theme">
                <div class="items">
                    <div class="table-cell">
                        <div class="wrapper">
                            <div class="row">
                                <div class="other-person">
                                    <div class="op1">
                                        <img src="img/dev.png" alt="">
                                    </div>
                                    <div class="op2">
                                        <img src="img/niz.png" alt="">
                                    </div>
                                </div>
                                <div class="person-text person2">
                                    <img src="img/inf.png" alt="">
                                    <div class="colored-text">
                                        <div class="text-black round">
                                            <p>Я - инфляция. Я ем твои заначки!
                                            <br><br>
                                            Инфляция – удручающее явление. Весной вы убираете в шкаф любимую шубу, а ближе к зиме обнаруживаете, что у нее пропал рукав. И швы неприятно изъедены. Так и действует инфляция, только вместо шубы ваши сбережения.
                                            Если год назад вы могли купить жвачку за 120 тенге, сегодня она может стоить уже 170. Так обесцениваются деньги, и дорожают все товары. Если ваш свободный капитал не работает, шансов угнаться за этим ростом цен нет никаких.
                                             </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="items">
                    <div class="table-cell">
                        <div class="wrapper">
                            <div class="row">
                                <div class="other-person">
                                    <div class="op1">
                                        <img src="img/inf.png" alt="">
                                    </div>
                                    <div class="op2">
                                        <img src="img/dev.png" alt="">
                                    </div>
                                </div>
                                <div class="person-text person3">
                                    <img src="img/niz.png" alt="">
                                    <div class="colored-text">
                                        <div class="text-black round">
                                            <p>Я - низкий доход. Я чувствую девальвацию!
                                            <br><br>
                                            Доходность по ряду долларовых депозитов казахстанских банков сегодня снизилась до 0,5% годовых. В среднем она не превышает 1%. Эти ставки вообще сложно назвать доходностью.
                                            Деньги на такие депозиты вносят исключительно для их сохранения и защиты от девальвации. А как же заработок?
                                             </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="items">
                    <div class="table-cell">
                        <div class="wrapper">
                            <div class="row">
                                <div class="other-person">
                                    <div class="op1">
                                        <img src="img/niz.png" alt="">
                                    </div>
                                    <div class="op2">
                                        <img src="img/inf.png" alt="">
                                    </div>
                                </div>
                                <div class="person-text person1">
                                    <img src="img/dev.png" alt="">
                                    <div class="colored-text">
                                        <div class="text-black">
                                            <p>Я – девальвация. Располовиню твои сбережения, глазом не успеешь моргнуть!
                                            <br><br>
                                            Девальвация – ослабление курса нац. валюты к другим «твердым» валютам и соответствующая потеря ее покупательной способности, например, в отношении импортных товаров. Нередко происходит резко и является существенным стресс-тестом для населения.
                                             </p>
                                        </div>
                                        <div class="text-orange">
                                            <p>За последние 5 лет Казахстан столкнулся с девальвацией дважды: обвалы тенге на 19% в 2014 году и 104% (!!!) в 2015-м.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->


        <!-- СМИ -->

        <div class="section question fp-normal-scroll" data-anchor="Вопросы">
            <div class="wrapper">
                <div class="row">
                    <div class="ques">
                        <div class="heading">
                            <span>Вопросы/ответы</span>
                        </div>
                        <div class="ques-accordeon">
                            <!-- <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        1
                                    </div>
                                    <div class="acc-text">
                                        Почему доходность снижается до 7%?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>
                                        Фиксированная доходность (к погашению) по бондам АО «Фридом Финанс» FFINb3 будет снижена до 7% с октября 2018 года в связи с тем, что руководство компании не видит необходимости в привлечении средств в ускоренном режиме. <br><br>
                                        Привлечение средств для развития в соответствии со стратегией компании проходит согласно плану, поэтому потребности в стимулировании спроса путем предложения завышенной доходности сейчас нет. <br><br>
                                        Для инвесторов, вложивших в бонды FFINb3 ранее, а также для всех, кто успеет приобрести данные бумаги до конца сентября 2018 года, условия сохраняются без изменений: такие инвесторы будут по-прежнему получать фиксированную доходность 8% годовых в долларах до погашения бумаг в 2021 году. <br><br>
                                        Отметим: доходность 7% годовых в долларах США – это в 14 раз выше, чем предлагают банки по валютным депозитам.
                                    </p>
                                </div>
                            </div> -->
                            <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        1
                                    </div>
                                    <div class="acc-text">
                                        Бонды – это облигации… Облигации, говорите? Мы это уже проходили, когда вся Россия, вложившись в облигации, в 98-м осталась у разбитого корыта.
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Мы не отрицаем, что в истории были случаи неправильного использования и даже мошенничества с бондами. В упомянутой ситуации российское правительство вообще сформировало настоящую финансовую пирамиду. Но нужно понимать, что это аномалия. Доходность гос. облигаций тогда в моменте превышала 250%. Любой эксперт в области инвестиций скажет вам, что это не нормально и чрезвычайно рискованно. При этом вовлеченность инвесторов была сумасшедшей: участвовали крупные банки, пенсионные фонды, страховые компании, предприятия. Одни только иностранцы вложили в эти бумаги порядка $6 млрд. И это при том, что было видно – правительство уже даже долг, привлеченный внутри страны, не могло обслуживать.
                                        <br><br>
                                        Все это было, с этим не поспоришь.
                                        <br><br>
                                        Но не стоит из-за неграмотного/мошеннического применения в конкретном случае негативно воспринимать бонды в принципе. Облигации – это совершенно законный и широко распространенный инвестиционный инструмент. Размер мирового рынка облигаций превышает $100 трлн (Вы можете проверить это во множественных источниках). На заметку, капитализация глобального рынка акций составляет порядка $76 трлн, т.е. он меньше.
                                        <br><br>
                                        Также бонды традиционно считаются надежнее акций.
                                        <br><br>
                                        Бонды выпускают компании, правительства стран, городские и местные власти. На американском фондовом рынке размещено порядка 109 тыс. различных наименований корпоративных бондов. Среди компаний, выпустивших их такие имена, как Apple, Microsoft, Amazon, Disney, Johnson&Johnson и т.д.
                                        <br><br>
                                        На казахстанской фондовой бирже торгуется 218 наименований корпоративных бондов. Среди эмитентов: «Народный банк», «Казахтелеком», НК «КазМунайГаз», Банк «ЦентрКредит», «Қазақстан Темір Жолы» и многие другие гиганты отечественного бизнеса.
                                        <br><br>
                                        Что касается бондов АО «Фридом Финанс», объем выпуска данных бумаг четко определен заранее. Т.е. речи об излишнем непродуманном привлечении средств, создании финансовой пирамиды и дальнейшей неспособности рассчитаться с инвесторами и быть не может.
                                        <br><br>
                                        Финансовые результаты и положение компании позволяют добросовестно выполнять обязательства, что подтверждается результатами обращения <a href="">первого выпуска аналогичных бондов</a>.
                                        <br><br>
                                        Привлеченные средства будут использованы в соответствии со стратегией развития компании и позволят добиться дальнейшего роста доходов.
                                    </p>
                                </div>
                            </div>
                            <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        2
                                    </div>
                                    <div class="acc-text">
                                        На депозите мои деньги защищены государством. До 10 млн тенге мне гарантированно вернут. Какие гарантии у вас?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Основным подтверждением надежности вложений в наши бонды является масштаб и устойчивость группы компаний Freedom Holding Corp. Собственный капитал группы составляет более $115 000 000, что вдвое превышает общий объем всей нашей облигационной программы, состоящей из трех выпусков индексированных бондов и рассчитанной на несколько лет.
                                        <br><br>
                                        Цифры также подкрепляются прочной репутацией компании «Фридом Финанс». На сегодняшний день мы являемся системообразующим участником фондового рынка в Казахстане, Кыргызстане, Узбекистане и Украине, а также в целом занимаем лидирующие позиции среди брокерских организаций СНГ.
                                        <br><br>
                                        Компания регулируется Национальным банком РК и имеет все необходимые лицензии на осуществление деятельности на рынке ценных бумаг.
                                        <br><br>
                                        Бонды «Фридом Финанс» <a data-fancybox  href="docs/Svidet.pdf">зарегистрированы</a> Нац. банком РК.
                                        <br><br>
                                        Вся информация о данных бумагах находится в открытом доступе на сайте <a href="https://kase.kz/ru/bonds/show/FFINb3/" target="blank">Казахстанской фондовой биржи (KASE)</a>.
                                        <br><br>
                                        Вы можете ознакомиться со всеми деталями, изучив подробный <a data-fancybox href="docs/Prospect.pdf">проспект  выпуска облигаций</a>.
                                        Также важно учитывать, что при работе на фондовом рынке с АО «Фридом Финанс»:</p>
                                    <ul>
                                        <li><p>Ваши финансовые средства и ценные бумаги хранятся на Вашем личном счете в Центральном депозитарии (организации, подконтрольной Нац. банку РК)</p></li>
                                        <li><p>Собственные средства «Фридом Финанс» отделены от средств клиентов, что позволяет избегать дополнительных рисков.</p></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        3
                                    </div>
                                    <div class="acc-text">
                                        Ваши бонды позиционируются, как «необеспеченные» - это плохо?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Понятие «необеспеченные» значит, что данные бонды не привязаны к конкретному физическому активу, который может быть продан для выплаты долгов.
                                        <br><br>
                                        Тем не менее, компания «Фридом Финанс» отвечает за исполнение обязательств своей прочной репутацией, высокой финансовой устойчивостью и большими достижениями.
                                        <br><br>
                                        Список активов компании включает в себя крупные пакеты акций АО «Kселл» (9%), АО «KEGOC» (4%), АО «Казахтелеком».
                                    </p>
                                </div>
                            </div>
                            <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        4
                                    </div>
                                    <div class="acc-text">
                                        Могу ли я потерять все деньги на ваших бондах?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Бонды действительно являются инструментом инвестирования, а не сбережений. Соответственно, определенные риски есть. Но нужно помнить, что наличие риска означает также наличие возможностей.
                                        <br><br>
                                        Например, возможности получить больший доход. Также бонды АО «Фридом Финанс» изначально несут в себе возможность защитить Ваши средства от девальвации тенге, что является важным преимуществом и снижает другие существенные риски.
                                        <br><br>
                                        Потенциальный негатив всегда можно сгладить даже самым простым подходом к управлению риском.
                                        <br><br>
                                        Взять хотя бы классическое правило «не кладите все яйца в одну корзину», т.е. распределяйте свой капитал между несколькими идеями. Так даже при самом экстремальном сценарии (к которому с бондами «Фридом Финанс» нет никаких предпосылок) Вы потеряете лишь часть инвестированных средств.
                                        <br><br>
                                        Существуют различные простые стратегии определения доли бондов в инвестиционном портфеле. Следование любой из них предполагает вложение в бонды лишь части ваших свободных средств.
                                        <br><br>
                                        Дополнительно стоит учесть, что бонды АО «Фридом Финанс» выпускаются в соответствии c законом РК «О рынке ценных бумаг» и утвержденными Национальным Банком РК правилами государственной регистрации выпуска негосударственных облигаций.
                                        <br><br>
                                        Компания соответствует всем требованиям, предъявляемым законодательством и регулятором.
                                        <br><br>
                                        Финансовые результаты и положение компании позволяют добросовестно выполнять обязательства, что подтверждается результатами обращения первого <a href="https://kase.kz/ru/bonds/show/FFINb2/" target="blank">выпуска аналогичных бумаг</a>. </p>

                                </div>
                            </div>
                            <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        5
                                    </div>
                                    <div class="acc-text">
                                        На депозите мои сбережения растут по принципу «снежного кома» благодаря «капитализации», т.е. каждый год проценты начисляются на все большую сумму. С бондами так же?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Нет. В случае с бондами т.н. «сложный процент» не применим. В день выплаты Вы получаете купонное вознаграждение на свой брокерский счет. Сумма Ваших инвестиций в данные бонды не увеличивается автоматически. Однако Вы можете реинвестировать полученное купонное вознаграждение в эти же бумаги и таким образом получать с каждым разом все большую сумму в виде регулярных купонных выплат.</p>

                                </div>
                            </div>
                            <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        6
                                    </div>
                                    <div class="acc-text">
                                        Могут ли мои бонды украсть?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Нет. Сегодня владение ценными бумагами не предполагает держания на руках физических сертификатов. Ваши бонды закрепляются за Вами и хранятся в электронном виде в «Центральном депозитарии ценных бумаг» - некоммерческой организации, основными акционерами которой являются Нац. банк РК (54,97% акций) и биржа KASE (34,69% акций). </p>

                                </div>
                            </div>
                            <!-- <div class="acc-every">
                                <div class="acc-h">
                                    <div class="acc-num">
                                        7
                                    </div>
                                    <div class="acc-text">
                                        На бирже KASE много бумаг с более высокой доходностью – как корпоративных, так и государственных. Чем ваши бонды лучше?
                                    </div>
                                    <div class="acc-arrow">
                                        <span></span>
                                    </div>
                                </div>
                                <div class="acc-body">
                                    <p>Действительно, облигаций с большей доходностью не мало. Однако аналогичных условий по защите от валютных рисков не предлагают практически ни одни из них. Стоит ли гнаться за высоким купонным доходом, когда потенциальная девальвация может в итоге располовинить всю Вашу инвестицию?
                                    <br><br>
                                    Средняя купонная доходность корпоративных облигаций на KASE – приблизительно 8,93% годовых. Наши бонды примерно соответствуют ей и при этом дают гораздо лучшие условия.
                                    <br><br>
                                    По государственным бумагам средняя доходность ниже – менее 7% годовых. При этом многие гос. облигации, предлагающие доходность свыше 8% имеют срок обращения от 10 лет, т.е. они подвержены большей неопределенности – никто не знает, что будет через 10 и тем более через 30 лет.
                                    </p>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="section forma fp-normal-scroll" id="forma" done="0" data-anchor="Форма">
            <div class="wrapper">
                <div class="row">
                    <div class="forma-content">
                        <h2>Дочитали до этого момента?  </h2>
                        <span>Вас точно заинтересовали бонды!</span>
                        <div class="mob-type-nurtas">
                            <img src="img/na4.png" alt="">
                        </div>
                        <div class="mob-type-form">
                            <p>Оставьте заявку, и мы  проконсультируем Вас <br> по всем вопросам</p>
                            <form action="" class="form">
                                <div class="req-inputs">
                                    <input class="hfield" type="text" placeholder="Не запoлнять" name="NAME">
                                    <input class="hfield" type="text" placeholder="Не запoлнять" name="EMAIL">
                                    <input class="hfield" type="text" placeholder="Не запoлнять" name="COMMENT">
                                </div>
                                <input required type="text" placeholder="ФИО" class="input1 form-fio" name="form[fio]">
                                <input required type="text" placeholder="Телефон" class="input1 tele form-phone" name="form[tfn]">
                                <input required type="email" placeholder="Email" class="input1 form-email" name="form[eml]">
                                <button>Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="section art fp-normal-scroll" id="forma" done="0" data-anchor="Форма">
            <div class="wrapper">
                <div class="col-6">
                    <div class="art__content">
                        <!--<p class="art-thintitle md-no-br">Хотите лучше разбираться в финансах и инвестициях, <br>чтобы самостоятельно понимать преимущества бондов?</p>
                        <h1 class="art-title">ИСКУССТВО <br>ИНВЕСТИРОВАНИЯ</h1>
                        <p class="art-thintitle">Бесплатный мастер-класс <br>Доступнен во всех регионах Казахстана</p>
                        <a href="https://art.ffin.kz" class="btn art__more" target="_blank">Узнать подробнее</a>-->
                        <p class="art-thintitle md-no-br">Дочитали до этого момента? Вам точно интересны бонды! </p>
                        <h1 class="art-title">Оставьте заявку</h1>
                        <p class="art-thintitle">и мы ответим на все вопросы!</p>
                    </div>
                </div>
                <div class="col-6">
                    <div class="art__form">
                        <h3 class="art__form-title">Купить бонды</h3>
                        <form action="" class="mons-form art-form">
                            <div class="req-inputs">
                                <input class="hfield" type="text" placeholder="Не запoлнять" name="NAME">
                                <input class="hfield" type="text" placeholder="Не запoлнять" name="EMAIL">
                                <input class="hfield" type="text" placeholder="Не запoлнять" name="COMMENT">
                            </div>
                            <input required="" type="text" placeholder="ФИО" class="input1 form-fio" name="full_name">
                            <input required="" type="tel" placeholder="Телефон" class="input1 tele form-phone" name="phone">
                            <input required="" type="email" placeholder="Email" class="input1 form-email" name="email">
                            <button class="btn art__btn">Купить</button>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section fp-normal-scroll cities" data-anchor="Контакты">
            <!-- <div class="wrapper">
                <div class="row">
                    <div class="contacts">
                        <div class="heading">
                            <span>контакты</span>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="mobile-map">
                <div class="row">
                    <div class="mob-map">

                    </div>
                </div>
            </div>
            <div class="map">
                <div id="map"></div>
                <div class="wrapper">
                    <div class="row">
                        <div id="contacts-info" class="map-inner">
                            <select name="" id="map_changer">
                                <option value="almaty" data-adress="Республика Казахстан, 050040, г. Алматы, проспект Аль-Фараби 77/7, Esentai Towers, 7 этаж" data-phone="<a href='tel:+7-727-311-10-64'>+7 727 311 10 64</a>" data-gps="76.929218,43.219217">Алматы</option>
                                <option value="astana" data-adress="ул. Кабанбай батыра 15А Бизнес центр &laquo;Q&raquo; Блок А, 3-й этаж" data-phone="<a href='tel:+7 7172 55 80 67'>+7 7172 55 80 67</a>, <a href='tel:+77172566924'>+7 7172 566 924</a>" data-gps="71.41652706417084,51.13458218119264">Астана</option>
                                <option value="aktau" data-adress="130000, г. Актау, 4-й микрорайон, д. 73, гостиничный комплекс Holiday Inn." data-phone="+7.7292.70.95.25, +7.7292.20.32.75, +7.7292.20.32.74" data-gps="51.165128305557005,43.638652006999045">Актау</option>
                                <option value="aktobe" data-adress="030000, г. Актобе, пр. Алии Молдагуловой, 46А, 5 этаж, офис №502" data-phone="+7.7132.70.37.74, +7.7132.74.07.69" data-gps="57.1441738506422,50.28586486865037">Актобе</option>
                                <option value="atyrau" data-adress="060000, г. Атырау, ул. Студенческая 52, БЦ «Адал» 2 этаж, 201 офис " data-phone="+7.7122.55.80.37" data-gps="51.914749329451475,47.1047924327959">Атырау</option>
                                <option value="karaganda" data-adress="100000, г. Караганда, ул. Ерубаева, д. 35, оф. 42 " data-phone="+7.7212.55.90.95" data-gps="73.08978999999997,49.80886000000573">Караганда</option>
                                <option value="kostanay" data-adress="110000, г. Костанай, пр-т Аль-Фараби, 65, 12 этаж, офис № 1201 " data-phone="+7.7142.99.00.53" data-gps="63.63365999999998,53.21922999999771">Костанай</option>
                                <option value="pavlodar" data-adress="140000, г. Павлодар, ул. Торайгырова 79/1, Бизнес-центр RESPECT, 1 этаж" data-phone="+7.7182.70.37.74" data-gps="">Павлодар</option>
                                <option value="semey" data-adress="г. Семей, ул. Дулатова, д. 167, кв. 120 " data-phone="+7.7222.56.00.60" data-gps="80.24672,50.41358000000412">Семей</option>
                                <option value="taraz" data-adress="080000, г. Тараз, ул. Толе би, д. 93а, 5 этаж, оф. 5.2" data-phone="+7.7262.99.98.97" data-gps="71.34558367820703,42.900043439529355">Тараз</option>
                                <option value="uralsk" data-adress="090000, г. Уральск, ул. Ескалиева, д.177, оф. 505" data-phone="+7.7112.55.47.23" data-gps="51.36882999999991,51.21522000000211">Уральск</option>
                                <option value="ustakaman" data-adress="070000, г. Усть-Каменогорск, ул. Максима Горького, д. 50" data-phone="+7.7232.56.96.03" data-gps="">Усть-Каменогорск</option>
                                <option value="shymkent" data-adress="160000, г. Шымкент, ул. Кунаева, д. 59, 1 этаж" data-phone="+7.7252.99.80.45" data-gps="69.60324790310786,42.335574514575626">Шымкент</option>
                                <option value="zhanaozen" data-adress="г. Жанаозен, микрорайон 3 А, Бизнес-центр &laquo;Максат&raquo; 2-этаж, 8 офис " data-phone="+7.777.862.9999" data-gps="">Жанаозен</option>
                            </select>
                            <div class="address">
                                <span>АО «Фридом Финанс» <br><br>Республика Казахстан, 050040, г. Алматы, проспект Аль-Фараби 77/7, Esentai Towers, 7 этаж</span>
                            </div>
                            <div class="call">
                                <p><span>Тел:</span><u><a href="tel:+7-727-311-10-64">+7 727 311 10 64</a></u></p>
                            </div>
                            <div class="call-center">
                                <span>CALL CENTER </span>
                                <p><a href="tel:8 800 080 3131">8 800 080 3131</a> (звонок бесплатный) <br>
                                    <a href="tel:7555">7555 </a>(бесплатно со всех мобильных операторов Казахстана)</p>
                            </div>
                            <div class="work">
                                <span>График работы CALL CENTER:</span>
                                <p>пн – пт 9.00 - 21.00 <br>
                                    сб – вс 9.00 - 18.00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper">
                <div class="row">
                    <div class="footing">
                        <div class="foot1">
                            <div class="foot-logo">
                                <a href="">
                                    <span></span>
                                </a>
                            </div>
                            <div class="domen-address">
                                <a href="tel:7555">7555 |</a>
                                <a href="mailto:bonds@ffin.kz">bonds@ffin.kz</a>
                            </div>
                            <div class="foot-address">
                                <span>Казахстан, г. Алматы, пр-т аль-Фараби, 77/7, <br>Esentai Towers, 7 этаж</span>
                            </div>
                        </div>
                        <div class="foot2">
                            <span>Меню</span>
                            <ul id="">
                                <li data-menuanchor="Бонды" class="menu7"><a href="#Бонды">Бонды - что это?</a></li>
                                <li data-menuanchor="Решение" class="menu2"><a href="#Решение">Параметры</a></li>
                                <li data-menuanchor="Инвестировать" class="menu3"><a href="#Инвестировать">Как инвестировать</a></li>
                                <li data-menuanchor="Компания" class="menu4"><a href="#Компания">О компании</a></li>
                                <!-- <li data-menuanchor="Кошмары" class="active menu1"><a href="#Кошмары">Кошмары</a></li> -->
                                <li data-menuanchor="Вопросы" class="menu5"><a href="#Вопросы">Вопрос/ответ</a></li>
                                <li data-menuanchor="Контакты" class="menu6"><a href="#Контакты">Контакты</a></li>
                            </ul>
                        </div>
                        <div class="foot3">
                            <span>Социальные сети</span>
                            <ul>
                                <li><a href="https://www.facebook.com/ffin.kz/">Facebook</a></li>
                                <li><a href="https://www.youtube.com/channel/UC72PQ_jRLUKbJmrWC8R6sqQ">Youtube</a></li>
                                <li><a href="https://ffin-edu.com">«Образовательный центр»</a></li>
                                <li><a href="https://ffin-edu.com">ffin-edu.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="footing2">
                        <div class="ft1">
                            <span>© 2019 АО «Фридом Финанс»</span>
                        </div>
                        <!-- <div class="ft2">
                            <span>Разработано <a href="">SlonWorks</a></span>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="popup1">
        <div class="push">
            <div class="table-cell">
                <div class="wrapper">
                    <div class="row">
                        <div class="thank">
                            <div class="thank1">
                                <span>cпасибо за заявку!</span>
                                <p>С вами свяжется наш специалист</p>
                                <a onclick="$('.popup1').fadeOut('slow')">Назад</a>
                            </div>
                            <div class="thank2">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>


<img src="img/na3-2.png" alt="" class="hidden-popup-img">
<!--REMODAL POPUP PART-->
<div class="remodal-bg">
    <div id="qunit"></div>
</div>

<!--a href="" data-remodal-target="modal">Modal</a-->
<div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking: false">
    <div class="remodal-content">
        <div class="row">

        </div>
    </div>
    <a data-remodal-action="close" class="remodal-close"></a>
</div>


<!-- settimeout. функция вызова выполняется в preloader -->
<!-- <div class="bond-pop-up mon1" id="bond-pop-up" style="display: none;">
    <b>Успейте зафиксировать 8% там, где другие получат 7%</b><br><br>
    <p>
        Фиксированная доходность (к погашению) по бондам АО «Фридом Финанс» FFINb3 будет снижена до 7% с октября 2018 года в связи с тем, что руководство компании не видит необходимости в привлечении средств в ускоренном режиме. <br><br>
        Привлечение средств для развития в соответствии со стратегией компании проходит согласно плану, поэтому потребности в стимулировании спроса путем предложения завышенной доходности сейчас нет. <br><br>
        Для инвесторов, вложивших в бонды FFINb3 ранее, а также для всех, кто успеет приобрести данные бумаги до конца сентября 2018 года, условия сохраняются без изменений: такие инвесторы будут по-прежнему получать фиксированную доходность 8% годовых в долларах до погашения бумаг в 2021 году. <br><br>
        Отметим: доходность 7% годовых в долларах США – это в 14 раз выше, чем предлагают банки по валютным депозитам.
    </p>
</div> -->

<!--REMODAL POPUP PART END-->

<script>
    new WOW(
        {
            offset: 0,
        }
    ).init();
</script>

<script type="text/javascript">
    $( ".form" ).submit(function( event ) {

        var $form = $(this);

        $form.find('button').prop('disabled',true);

        // Use Ajax to submit form data
        $.get('/api/add-form', $form.serialize(), function (result) {
            $form.find("input, textarea").val("");
            $('.popup1').fadeIn('fast');
        }, 'json').fail(function ()
        {
            alert('Произошла ошибка. Попробуйте еще раз.');
        });

        $.fancybox.close();
        event.preventDefault();
    });

    $( ".art-form" ).submit(function( event ) {

        var $form = $(this);

        $form.find('button').prop('disabled',true);

        // Use Ajax to submit form data
        $.get('/api/add-form', $form.serialize(), function (result) {
            $form.find("input, textarea").val("");
            $('.popup1').fadeIn('fast');
        }, 'json').fail(function ( jqXHR, textStatus, errorThrown)
        {
            alert('Произошла ошибка. Попробуйте еще раз.');
        });


        event.preventDefault();
    });
</script>


<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script type="text/javascript" src="js/map.js?v=240720181016"></script>









</body>
</html>
