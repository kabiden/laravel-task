@extends('admin.main')

@section('title', 'Список заявок')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12 mb-5">
                <form id="frm-logout" action="{{ route('logout') }}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Выйти</button>
                </form>
            </div>
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ФИО</th>
                        <th>Номер</th>
                        <th>Почта</th>
                        <th>Дата</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($forms as $form)
                        <tr>
                            <td>{{ $form->id }}</td>
                            <td>{{ $form->full_name }}</td>
                            <td>{{ $form->phone }}</td>
                            <td>{{ $form->email }}</td>
                            <td>{{ $form->created_at }}</td>
                            <td>
                                <form action="{{ route('delete', ['id' => $form->id]) }}" method="post">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-link"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
