@extends('admin.main')

@section('title', 'Список заявок')

@section('content')

    <div class="wrapper fadeInDown">
        <div id="formContent">
            <div class="fadeIn first">
                <h3>Авторизация</h3>
            </div>
            <!-- Login Form -->
            <form action="{{ route('auth') }}" method="POST">
                {{ csrf_field() }}
                <input type="text" id="login" class="fadeIn second" name="email" placeholder="Логин">
                <input type="password" id="password" class="fadeIn third" name="password" placeholder="Пароль">
                <input type="submit" class="fadeIn fourth" value="Войти">
            </form>

        </div>
    </div>
@endsection
